{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "08383070-a268-4b55-b9cb-78c00d5a8ccc",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "# How to work with matrices\n",
    "\n",
    "Our goal is to analyse MEG data. This kind of data is commonly represented\n",
    "as a matrix. A matrix has a certain number of rows and columns. Each cell\n",
    "contains one number which in the case of MEG data is the current magnetic\n",
    "field in either Tesla (for magnetometers) or Tesla/meter for gradiometers.\n",
    "Each row of the matrix is one channel and each column is one sample.\n",
    "Treating the data as a matrix has one more advantage (besides being quite\n",
    "intuitive): Calculations on matrices are quite fast on computers because\n",
    "algorithms and even the CPUs themselves are optimized to operate on matrices.\n",
    "\n",
    "Unfortunately, working with matrices is NOT part of Python itself. However,\n",
    "Python can be extended using so-called \"packages\".\n",
    "In order to use packages, we need to\n",
    "\n",
    "1. Install them. In your case, this has already been done. Remember when you\n",
    "   prepared your laptop and it started downloading and installing lots\n",
    "   stuff after you typed in the conda create... command? Those were all\n",
    "   packages that were installed.\n",
    "2. Import the packages you actually need in your script. This is done\n",
    "   with the import statement.\n",
    "\n",
    "The package we need for matrix operation is called numpy. There is a\n",
    "convention to \"rename\" it in scripts, this is what the \"as np\" part is for:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b47e8d1a-a1c9-4a70-bbb9-3eadfdca2391",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b24bc41c-f965-46eb-8f6d-9727d104b86a",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Now we can use all the functions of numpy in our script. You can take a look\n",
    "at the official documentation here: https://docs.scipy.org/doc/numpy/reference/\n",
    "to see what you can do with it. I will just give a few examples here.\n",
    "\n",
    "The first thing we need to do is make a matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1f386e4c-afe4-4fda-9acf-6fbdda592b81",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "my_matrix = np.array([[1, 2, 3],\n",
    "                      [4, 5, 6],\n",
    "                      [7, 8, 9],\n",
    "                      [10, 11, 12]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a10d878-829c-4e0d-982f-f29fcc7acbde",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "What happens here is that we create a list that contains a list for each row\n",
    "of the matrix we want to create. We then call np.array with that list and\n",
    "get a matrix with four rows and three columns in return.\n",
    "If we want to know how big our matrix is, we can ask for its shape\n",
    "property:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9150fb3-ad70-436f-a9d0-af1499085183",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "my_matrix.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01fe9a81-dd41-4dd4-ac9a-042f0cd6ecac",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "So the matrix has 4 rows and three columns.\n",
    "\n",
    "We can also print the whole matrix. This works well for smaller matrices, but not so well for bigger ones:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af44fc5b-8b08-48b4-9bda-81c8c1e0b429",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "print(my_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c648b08a-5140-4fa2-a8a0-db5d4bdd4bd5",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "You can access the elements of the matrix using the `[]` notation you already\n",
    "know from lists and dictionaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b5d7701-7919-4e9b-9d1c-beb40a13965d",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "print(my_matrix[0]) # The first row\n",
    "print(my_matrix[0, 0]) # The element in the first row and first column\n",
    "print(my_matrix[1, 2]) # Second row, third column\n",
    "print(my_matrix[0:3, 2]) # Third column, the first three elements\n",
    "print(my_matrix[:, 1]) # All elements of the second column"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43fab84f-fa09-4b30-bf4c-a3dc104d4501",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Numpy also offers lots of very convenient methods for these array/matrices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7e54899-dbfe-4184-ade4-086b2b7c51b2",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "print(my_matrix.mean()) # The mean of all elements\n",
    "print(my_matrix.mean(axis=0)) # Mean over all rows\n",
    "print(my_matrix.mean(axis=1)) # Mean over columns\n",
    "\n",
    "print(my_matrix.sum())\n",
    "print(my_matrix.sum(axis=0))\n",
    "print(my_matrix.sum(axis=1))\n",
    "\n",
    "print(my_matrix + 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05b39b08-e96e-4f4e-b982-c2594e28dde4",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Interlude: The difference between assigning and copying\n",
    "\n",
    "In most cases, python variables do NOT contain the actual values but just\n",
    "the address of the element in the RAM of the computer. This is the case\n",
    "for all variables that are more complex than holding a single value or a\n",
    "single string.\n",
    "I.e. it is the case for:\n",
    "\n",
    "* lists\n",
    "* dictionaries\n",
    "* matrices\n",
    "* any MNE python object we are going to use in the next script.\n",
    "\n",
    "Here is an example"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4933c26b-85c7-42ce-b9d8-6244459e4fa6",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "first_list = [1, 2, 3, 4]\n",
    "second_list = first_list\n",
    "second_list[2] = 999\n",
    "\n",
    "print(first_list)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "713a69cd-a06f-4364-a0b9-6f6d21358c07",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "You can see that although we modified second_list, we see the change\n",
    "even when we look at first_list.\n",
    "The reason is that when we did this:\n",
    "```python\n",
    "second_list = first_list\n",
    "```\n",
    "\n",
    "We only said: \"second_list shall point to the same list as first_list\"\n",
    "\n",
    "If you want a real copy of an object (i.e. list, dictionary, matrix),\n",
    "you can either see whether that object has a copy method like the matrices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aecd27d4-617d-42d8-8e32-b387166d54e2",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "copied_matrix = my_matrix.copy()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d287e42-abcc-4e5f-9305-7d4e2bcfed5b",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Or if they do not, you can use the copy package of python, which you need\n",
    "to import first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1667fc01",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "import copy\n",
    "\n",
    "third_list = copy.copy(first_list)\n",
    "third_list[2] = 666\n",
    "\n",
    "print(first_list)\n",
    "print(third_list)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
