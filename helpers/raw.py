import numpy as np
import pandas as pd
from copy import deepcopy

from obob_mne.mixins.raw import AutomaticBinaryEvents, LoadFromSinuhe


class AutomaticBinaryEventsWithMetadata(AutomaticBinaryEvents):
    def _process_events(self):
        super(AutomaticBinaryEvents, self)._process_events()

        conditions_metadata = {}
        all_raw_metadata = []

        if self.condition_triggers:
            condition_trigger = self._events[0, 2]
            self._events = np.delete(self._events, (0), axis=0)

            for allcond_key, allcond_value in self.condition_triggers.items():
                conditions_metadata[allcond_key] = self._decode_bin_trigger(
                    condition_trigger,
                    allcond_value
                )

        for cur_evt_code in self._events[:, 2]:
            cur_metadata = deepcopy(conditions_metadata)
            for stim_group_name, stim_group_choices in \
                    self.stimulus_triggers.items():
                cur_metadata[stim_group_name] = self._decode_bin_trigger(
                    cur_evt_code, stim_group_choices
                )

            all_raw_metadata.append(cur_metadata)


        self._evt_metadata = pd.DataFrame(all_raw_metadata)


class RawSinuhe(LoadFromSinuhe, AutomaticBinaryEventsWithMetadata):
    study_acronym = 'th_music_markov'
    file_glob_patterns = ['%s_markov_%02d.fif']
    excluded_subjects = ['19970203urmr',
                         '19950719mrmd',
                         '20001006bagm']

    condition_triggers = {
        'condition_detuned': 1,
        'markov': {
            'random': 2,
            'ordered': 4
        }
    }

    stimulus_triggers = {
        'freq': {
            1: 16,
            2: 32,
            3: 64,
            4: 128
        },
        'tone_detuned': 1
    }
    
    
class Raw(AutomaticBinaryEventsWithMetadata):
    condition_triggers = {
        'condition_detuned': 1,
        'markov': {
            'random': 2,
            'ordered': 4
        }
    }

    stimulus_triggers = {
        'freq': {
            1: 16,
            2: 32,
            3: 64,
            4: 128
        },
        'tone_detuned': 1
    }