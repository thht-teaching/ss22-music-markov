import json
from pathlib import Path
import openpyxl
import numpy as np

translate_map = str.maketrans('ş', 's')
subject_id_corrections = {
    '19630827mrrb': '19960717mrrb'
}

def get_gms_data():
    behav_json = Path('/home/th/git/analysis/markov_music/data_in_git/jatos_results_20220519131006.json')
    gms_xlsx_file = Path('/home/th/git/analysis/markov_music/data_in_git/gms_full_scoring_template_en.xlsx')

    raw_string = behav_json.read_text()
    fixed_string = '[' + ','.join(raw_string.split('\n'))[:-1] + ']'

    raw_behav_data = json.loads(fixed_string)

    gms_xlsx = openpyxl.load_workbook(gms_xlsx_file)
    master_ws = gms_xlsx['Master']
    subscale_cols = ('BA', 'BB', 'BC', 'BD', 'BE', 'BF')

    subscales = {}
    max_question = 0
    for cur_col in subscale_cols:
        subscale_name = master_ws[f'{cur_col}1'].value
        subscale_formular = master_ws[f'{cur_col}2'].value
        subscale_values = subscale_formular[5:-1].split(',')
        question_idx = [master_ws[x].column - 1 for x in subscale_values]
        question_idx.sort()
        subscales[subscale_name] = np.array(question_idx)
        max_question = max(max_question, max(question_idx))

    subscales['total'] = np.arange(max_question) + 1
    subscales['years_instrument_practise'] = np.array([32])
    subscales['hrs_instrument_practise_per_day_at_peak'] = np.array([33])
    subscales['nr_live_events'] = np.array([34])
    subscales['years_music_theory'] = np.array([35])
    subscales['years_instrument_education'] = np.array([36])
    subscales['nr_of_instruments'] = np.array([37])
    subscales['minutes_music_listening_per_day'] = np.array([38])

    gms_scores = {}
    for cur_behav in raw_behav_data:
        subject_id = cur_behav[2]['subject_id'].translate(translate_map)
        if subject_id in subject_id_corrections:
            subject_id = subject_id_corrections[subject_id]

        quest_data = cur_behav[3]['questionaire_data']
        scores = np.array([x['processed_score'] for x in quest_data])

        subscale_scores = {}

        for subscale, q_idxs in subscales.items():
            subscale_scores[subscale] = scores[q_idxs - 1].sum()

        gms_scores[subject_id] = {
            'raw_scores': scores,
            'subscale_scores': subscale_scores,
        }

    return gms_scores