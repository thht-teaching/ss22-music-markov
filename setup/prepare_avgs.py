#%%import

import mne
from pathlib import Path
import hashlib
#%%
# set variables
src_path = Path('/mnt/obob/staff/thartmann/experiments/music_markov/data/03_extracted_epochs')
output_path = Path('data/avgs')

output_path.mkdir(exist_ok=True)
#%%
# get all files
all_files = src_path.rglob('*.fif')
#%%
# iterate through all files
for cur_file in all_files:
    subject_id = cur_file.name[:12]

    new_fname = f'{hashlib.md5(subject_id.encode()).hexdigest()}.fif'
    output_file = Path(output_path, new_fname)

    if not output_file.exists():
        data = mne.read_epochs(cur_file, proj=False)
        avg = data.average()
        avg.save(output_file)