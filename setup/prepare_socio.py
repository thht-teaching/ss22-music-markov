#%% import

import pandas as pd
import hashlib
from pathlib import Path

#%% set variables
input = Path('/home/th/Downloads/music_markov.xls')
output = Path('data/socioecon.xls')

#%% load
data = pd.read_excel(input)

#%% hash subjects
for idx_row in range(15, len(data)):
    old_id = data.iloc[idx_row, 1]
    new_id = hashlib.md5(old_id.encode()).hexdigest()

    data.iloc[idx_row, 1] = new_id

#%% drop not needed cols....
data.drop(data.columns[5:], axis=1, inplace=True)

#%% save
data.to_excel(output)

