#%% import
from helpers.behav import get_gms_data
import pandas as pd
from pathlib import Path
import hashlib

#%% set variables
output = Path('data/behav.xls')

#%% get data
behav_data = get_gms_data()

#%% convert data to pandas dataframe....
list_for_pd = []

for subject_id, data in behav_data.items():
    tmp_dict = {
        'subject_id': hashlib.md5(subject_id.encode()).hexdigest()
    }

    tmp_dict.update(data['subscale_scores'])

    list_for_pd.append(tmp_dict)

df = pd.DataFrame.from_records(list_for_pd)

#%% save
df.to_excel(output)