#%% imports
from pathlib import Path
import shutil
import hashlib

#%% set variables
source = Path('/mnt/obob/staff/thartmann/experiments/music_markov/data/01_decode_mags_trans')
output_path = Path('data/decoded')

output_path.mkdir(exist_ok=True)

#%% get all files...
all_files = source.rglob('*__trans_maxfilter_no.dat')

for cur_file in all_files:
    old_subject_id = cur_file.name[:12]
    new_subject_id = hashlib.md5(old_subject_id.encode()).hexdigest()
    new_name = cur_file.name.replace(old_subject_id, new_subject_id)

    if not Path(output_path, new_name).exists():
        shutil.copy(cur_file, Path(output_path, new_name))
