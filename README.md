# How to prepare your computer for the analysis session

Please follow these instructions and download everything before you come to the class so we all have a smooth experience.

I tried to make the process as smooth as possible. However, if you have problems, just contact me on monday and we sort it out.

You are going to need ~8GB of free disk space to do this...

## Step 0
If you are already familiar with anaconda python environments and maybe even git:

1. clone this repository. be sure to have installed git-lfs!
2. create the environment
3. launch `jupyter-lab` and you are good to go. Skip to step 5 to check whether all is good!

If the above is all gibberish to you, go to Step 1!

## Step 1: Download MNE-Python

Go here: https://mne.tools/stable/install/installers.html and download the installer for you operating system. Install it and always choose the default options.

## Step 2: Download the code and the data

1. Click this link: https://gitlab.com/thht-teaching/ss22-music-markov/-/archive/main/ss22-music-markov-main.zip
2. Wait for the download to finish
3. Extract the contents of this zip file to a new folder in your personal folder (Documents on Windows, no idea on mac)

## Step 3: Start Jupyter

Step 1 placed a new application in your start menu called `MNE (Prompt)`. Click that. It will open a terminal. It might take some time to initialize. Wait until it shows some text. If it does not do this after a couple of seconds, press some random keys. It actually might help.

As soon as the text is there, enter this command:

```bash
jupyter-lab
```

Then wait. Your web browser will shop up and it will look something like this:

![](https://gitlab.com/thht-teaching/ss22-music-markov/-/raw/main/img/jupyter_start.png)

## Step 4: Find the folder where you extracted the code and the data

On the left, you see a list of your folders. You can navigate them like normal. Now, navigate to the folder where you extracted the code and the data. It should now look like this:

![](https://gitlab.com/thht-teaching/ss22-music-markov/-/raw/main/img/folder_found.png)

Now, double-click on the file called `01_prepare.ipynb`. It will open like in a normal text editor. 

You now need to click on the icon that looks like this:

![](https://gitlab.com/thht-teaching/ss22-music-markov/-/raw/main/img/arrows.png)

It will ask you if you want to "restart the kernel". Confirm this, please.

Now wait. It can take up to one hour to install all the extra packages. When it is done, it will all of a sudden show lots of text.

When you think it is done, scroll down and make sure, the bottom looks like this:

![](https://gitlab.com/thht-teaching/ss22-music-markov/-/raw/main/img/done.png)

## Step 5: Check if everything works

To verify that everything works, double-click on the file called `04_raw_files.ipynb` and then click the double arrow button again. Confirm the question and wait a couple of minutes.

It should open 6 new windows with brain plots in them.

If this all works. You can close everything and come to class on tuesday.

If something went wrong, get in contact with me and we try to sort it out!
